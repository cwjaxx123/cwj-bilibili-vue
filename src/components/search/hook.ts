import { } from 'vue'
import route from '@/routers/index'
/**
 * 搜索框搜索
 * @param text 
 * @param placeholder 
 */
export const SearchSubmit = (text: string, placeholder: string) => {
    text = text == '' ? placeholder : text
    route.push(
        {
            name: 'searchpage',
            query: { text }
        })
}