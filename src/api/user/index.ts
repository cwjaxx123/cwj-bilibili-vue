import axios from '@/api/axios'
import { type userInfo, type LoginUser, LoginMethodEnum } from './types'
import { type ResResult, CodeEnum } from '@/api/types'
import { ElNotification } from 'element-plus'
enum API {
  Login = 'user/login',

}
/**
 *  用户登录
 *  password密码为必须
 *  email和phone二选一
 * @param user 
 * @param method
 */
export const userLogin = async (user: LoginUser, method: LoginMethodEnum) => {
  // console.log('用户登录')
  const form = new FormData();
  
  form.append('password', user.password)
  form.append('LoginMethodEnum',method.toString())
  if (method == LoginMethodEnum.Phone) form.append('phone', user.phone as string)
  else if (method == LoginMethodEnum.Email) form.append('email', user.email as string)

  return await axios.post<any, ResResult>(API.Login, form)
   

} 