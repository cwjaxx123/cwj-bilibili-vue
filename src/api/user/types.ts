/**
 * 已经登录的用户类型
 * 后端返回
 */
export type userInfo={
    /**
     * 头像
     */
    profile:string,
    /**
     * token
     * 
     * 存储用户信息
     */
    token:string,
    /**
     * 登录时间
     */
    loginTime:Date
}
/**
 * 登录用户类型
 */
export type LoginUser = {
    password: string,
    email?: string,
    phone?: string
}

/**
 * 登录注册方式枚举
 * 手机号或邮箱
 */
export enum LoginMethodEnum {
    /**手机号 */
    Phone=1,
    /**邮箱 */
    Email=2
}