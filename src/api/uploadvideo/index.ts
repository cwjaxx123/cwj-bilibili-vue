import axios from "@/api/axios";
import md5 from "js-md5";
import { useUserStore } from '@/pinia/userStore'
import type { category, subarea, AllVideoUpSchedule, videoupSchedule, upform, upvideotype, upCover } from './types'
import { type ResResult, CodeEnum } from '@/api/types'
import { GetQueryUrlByObj } from '@/utils/dataUtils'
import { GetFileMd5Asnc, FilechunkSize, GetFileExtname, GetFileBasename } from '@/utils/FileUtils'

/**
 * 投稿视频相关的Api接口集合
 */
enum API {
    /**
     * 获取分区api接口
     */
    CategoryAndSubarea = '/UpVideo/CategoryAndSubarea',
    /**
     * 上传投稿表单api接口
     */
    uploadFrom = '/UpVideo/uploadFrom',
    /**
     * 上传视频封面切片api接口
     */
    uploadCoverChunk = '/UpVideo/UploadCoverChunk',
    /**
     * 合并视频切片封面api接口
     */
    uploadMerginCover = '/UpVideo/uploadMerginCover',
    /**
     * 上传视频切片api接口
     */
    uploadVideoChunk = '/UpVideo/uploadVideoChunk',
    /**
     * 合并视频切片api接口
     */
    uploadMerginVideo = '/UpVideo/uploadMerginVideo'
}
/**
 * 获取投稿视频上传分区
 * @returns 
 */
export const GetCategoryAndSubarea = async () => {
    return await axios.get<any, ResResult>(API.CategoryAndSubarea)
}
/**
 * 存在session的上传表单集合
 */
const upvideolistformMd5 = 'upvideolistformMd5'
/**
 * 上传投稿视频的表单
 * 成功则返回表单生成的md5字符串
 */
export const uploadFrom = async (form: upform) => {
    // console.log('表单上传')
    return await axios.post<any, ResResult>(API.uploadFrom, form)
        .then(res => {
            if (res.code === CodeEnum.Suceess) {
                //存入sessionStorage中备用
                const listjson = sessionStorage.getItem(upvideolistformMd5)
                const data = res.data as { md5: string, dateTime: Date }
                // console.log('表单'+JSON.stringify(data))
                if (listjson) {
                    const videolist = JSON.parse(listjson) as []
                    sessionStorage.setItem(upvideolistformMd5, JSON.stringify([...videolist, data.md5]))
                } else {
                    sessionStorage.setItem(upvideolistformMd5, JSON.stringify([data.md5]))
                }
                return data.md5
            }
        }
        )

}

/**
 * 视频封面上传
 * 
 * 成功则返回上传表单的MD5值
 * @param formMd5 
 * @param cover 
 */


export const uploadCover = async (formMd5: string, upCo: upCover) => {
    // console.log('封面上传', cover.size, FilechunkSize)
    //切片数量
    const chunkCount = Math.ceil(upCo.cover.size / FilechunkSize)
    /**
     * 每个封面切片api对象
     */
    //每个封面切片api对象
    const httpList: { url: string, form: FormData }[] = []
    for (let index = 0; index < chunkCount; index++) {
        const videoblob = upCo.cover.slice(index * FilechunkSize, Math.min(upCo.cover.size, (index + 1) * FilechunkSize))
        const form = new FormData()
        form.append('cover', videoblob)
        //设置封面切片上传的url
        // console.log(upCo.ChunkList)
        const obj = {
            Index: index + 1,
            Md5: upCo.ChunkList.find(cu => cu.index == index + 1)!.md5,
            FormMd5: formMd5,
            CoverMd5: upCo.md5
        }
        let queryStr = GetQueryUrlByObj(obj)

        httpList.push({
            form: form,
            url: API.uploadCoverChunk + queryStr
        })

    }
    //等待所有的封面切片上传完成后再合并封面
    return await Promise.all(httpList.map(hp => axios.post(hp.url, hp.form)))
        .then(async () => {
            const query = {
                formMd5,
                PicType: GetFileExtname(upCo.cover.name),
                ChunkCount: httpList.length,
                md5: upCo.md5
            }
            // console.log('合并封面')
            return await axios.post(API.uploadMerginCover, query)
        })
        .then(res => (res as any).data.formMd5 as string)


}
/**
 * 上传投稿视频列表
 * @param VideoUpSchedule 
 * @param ChangeVideoSchedule 
 */
export const uploadVideoList = async (VideoUpSchedule: AllVideoUpSchedule,
    ChangeVideoSchedule: Function) => {
    const httpList = []
    for (let i = 0; i < VideoUpSchedule.videos.length; i++) {

        httpList.push(uploadVideo(VideoUpSchedule.videos[i], ChangeVideoSchedule, VideoUpSchedule.formMd5, i + 1)
        )
    }

    // return await Promise.resolve(VideoUpSchedule.videos.length)
    return await Promise.all(httpList)
        .then(() => {
            // console.log('上传完成')
            return VideoUpSchedule.videos.length
        })
}


//#region 视频文件上传
/**
 * 是否暂停上传
 */
let isPause = false;
/**
 * 暂停视频上传
 * @returns 
 */
export const upVideoPause = () => isPause = true
/**
 * 继续视频上传
 */
export const upVideoContinue = () => isPause = false

/**
 * 采用分片上传每个视频
 * @param videoupSchedule 
 * @param ChangeVideoSchedule 
 * @param Index 视频在列表的排序
 */
const uploadVideo = async (videoupSchedule: videoupSchedule, ChangeVideoSchedule: Function, formMd5: string, Index: number) => {
    const video = videoupSchedule.video.video

    //视频分片数量
    const chunkCount = Math.ceil(video.size / FilechunkSize)
    //每个封面切片api对象
    // let s1 = 0
    for (let index = 0; index < chunkCount; index++) {
        /**
       * 使用promise实现暂停上传功能
       */
        await new Promise(res => {
            const tot = setInterval(() => {
                if (!isPause) {
                    clearInterval(tot)
                    res('')
                }
            }, 50)
        })

        const videoblob = video.slice(index * FilechunkSize, Math.min(video.size, (index + 1) * FilechunkSize))
        const form = new FormData()
        form.append('videoChunk', videoblob)

        //视频切片设置请求接口url
        let queryStr = GetQueryUrlByObj({
            Index: index + 1,
            md5: videoupSchedule?.video?.ChunkList?.find(vv => vv.index == index + 1)?.md5,
            VideoMd5: videoupSchedule.md5,
            formMd5
        })

        queryStr.substring(0, queryStr.length - 1)


        await axios.post<any, ResResult>(API.uploadVideoChunk + queryStr, form)
            .then(() => {
                // s1 += videoblob.size
                // console.log(s1)
                //改变上传进度
                ChangeVideoSchedule(videoblob.size, videoupSchedule.md5)
            })
    }
    // console.log('--------------------------')
    // console.log(s1)
    // console.log(video.size)
    //请求参数对象
    const obj = {
        formMd5,
        md5: videoupSchedule.md5,
        chunkCount,
        VideoType: GetFileExtname(video.name),
        name: GetFileBasename(video.name),
        size: Math.floor(video.size / 1024 / 1024),
        Index,
        duration: Math.ceil(videoupSchedule.video.meta.duration)
    }

    // console.log(obj)
    // let queryStr = GetQueryUrlByObj(obj)
    //发送合并视频切片请求
    return await axios.post(API.uploadMerginVideo, obj)

}

//#endregion