export type subarea = {
    id: number,
    name: string,
    synopsis?: string
}
export type category = {
    id: number,
    name: string,
    subareas: subarea[]
}


/**
 * 单个视频上传进度类型
 */
export type videoupSchedule = {

    /**
     * 视频文件
     */
    video: upVideo,
    /**
     * 已上传的大小
     */
    nowSize: number //,
    /**
     * 视频的md5
     */
    md5: string,  //
    /**
     * 是否上传完成
     */
    isOver: boolean, //
}
/**
 * 总的上传进度类型
 */
export type AllVideoUpSchedule = {
    formMd5: string,
    videos: videoupSchedule[],
    isOver: boolean
}

/**
 * 视频上传状态
 */
export enum Status {
    suceess,
    fail,
    begin,
    no,
    ing
}

/**
* 视频上传参数限制
*/
export const upVideoConfig = {
    /**
     * 支持的视频类型
     */
    video: {
        VideoTypes: ['.mp4', '.mkv', '.flv'],//
        /**
         * 总容量mb
         */
        VideoSizeMax: 20 * 1024,
        /**
         * 集数
         */
        videoCountMax: 200, // 
        /**
         * 单个视频最大容量mb
         */
        videoSize: 5 * 1024, //
        /**
         * 视频文件名正则表达式
         * 
         */
        videonamereg: /^[\u4e00-\u9fa5\(\)_a-zA-Z0-9]+$/,
        /**
         * 视频文件名最大长度
         */
        videonamelen: 100,
    },
    picture: {
        /**
         * 支持的图片格式
         */
        PictureTypes: ['.png', '.jpg', '.jpeg', '.webp'], //
        /**
        * 图片最大容量mb
        */
        pictureSize: 20,  //
        /**
         * 图片文件名正则表达式
         */
        picturenamereg: /^[\u4e00-\u9fa5\(\)_a-zA-Z0-9]+$/,
        /**
         * 文件名长度
         */
        picturenameCount: 100
    },
    form: {
        /**
         * 标题最大字数100
         */
        titleCount: 100, //

        /**
         * 视频简介字数
         */
        synopsisCount: 1000,
        /**
        * 标签最大个数
        */
        tagCount: 8,
        /**
         * 每个标签最大长度
         */
        tagLen: 7
    }

}
/**
* 上传视频的来源类型
*
*  
*/
export enum upvideotype {
    /**
     * 自制
     */
    self,
    /**
     * 转载
     */
    source
}
/**
* 上传表单类型
*/
export type upform = {
    /**
     * 标题
     */
    title: string,
    /**
     * 来源
     * 
     * 自制 转载
     */
    type: upvideotype,
    /**
     * 转载时 视频的来源
     */
    source?: string,

    subarea: number,
    category: number,
    /**
     * 视频标签集合
     */
    tags: string[],
    /**
     * 视频简介
     */
    synopsis: string
}
/**
*上传视频类型
*/
export type upVideo = {
    /**
        * 视频MD5
        */
    md5: string,
    /**
     * 视频文件
     */
    video: File,
    //视频的一些基本信息
    meta: {

        /**
         * 视频时长
         */
        duration: number
    },
    /**
    * 文件的每个切片的索引及其md5
    */
    ChunkList: FileChunkMd5[]
}
/**
* 每个文件切片的索引及其md5
*/
export type FileChunkMd5 = {
    index: number,
    md5: string,
}
/**
 * 视频封面类型
 */
export type upCover = {
    /**
     * 封面图片文件
     */
    cover: File,
    /**
     * 封面的md5值
     */
    md5: string,
    /**
     * 文件的每个切片的索引及其md5
     */
    ChunkList: FileChunkMd5[]
}