/**
 * 请求返回类型
 */
/**
 * 请求响应状态码枚举
 */
export enum CodeEnum{
    Suceess=2000,
    
    Fail = 2001,
    
    Error = 20002
}
/**
 * 响应数据格式
 */
export type ResResult={
    /**
     * 请求响应状态码
     */
    code:CodeEnum,
    /**
     * 返回的数据
     */
    data:any,
    /**
     * 提示信息
     */
    msg?:string
}

