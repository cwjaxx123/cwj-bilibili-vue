import axios from "axios";

import { useUserStore } from '@/pinia/userStore'
import { ElNotification } from 'element-plus'

/**
 * 错误代码枚举
 */
enum ErrEnum {
    ERR_NETWORK = 'ERR_NETWORK'
}
axios.defaults.baseURL = import.meta.env.VITE_FRONT_BASE_URL
//请求前设置请求头
axios.interceptors.request.use(

    config => {
        const userStore = useUserStore()
          //设置token
        if (userStore && userStore.isLogin) {
            config.headers.Authorization = userStore.user.token
            // console.log('pinia')
        } else if (sessionStorage.getItem('userInfo')) {
            config.headers.Authorization = JSON.parse(sessionStorage.getItem('userInfo') as string).token
            // console.log('sessionStorage')
        } else if (localStorage.getItem('Authorization')) {
            config.headers.Authorization = JSON.parse(localStorage.getItem('userInfo') as string).token
            // console.log('localStorage')
        }
        //    console.log(new Date().getTime());

        return config
    }
)
/**
 * 响应头
 */
axios.interceptors.response.use(res => res.data,
    err => {
        /**
         * 异常提示信息
         */
        let message = `<div>${err.message}</div>
                             <div><hr>${err.config.url}</div>`

        ElNotification({
            title: '请求错误',
            message,

            dangerouslyUseHTMLString: true,
            type: 'error',
            zIndex: 10098
        })
        return Promise.reject(err);
    }
)
export default axios;