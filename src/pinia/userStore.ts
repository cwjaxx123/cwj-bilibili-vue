import { defineStore } from 'pinia'
import type { userInfo } from '@/api/user/types'

/**
 * 用户状态pinia数据
 */
type userPinia = {
  token: string | null,
  profile:string ,
  loginTime:Date,
}
type piniaState = {
  user: userPinia
}
/**
 * 存储用户状态
 */
export const useUserStore = defineStore('useUserStore', {
  state: (): piniaState => {
    const userInfoStr = sessionStorage.getItem('userInfo') ?? localStorage.getItem('userInfo') 
     const u= userInfoStr? JSON.parse(userInfoStr) as userPinia : null!
    let user: userPinia = {
    ...u
    }
    return {
      user
    }
  },
  actions: {
    /**
     * 把登录用户存储在pinia中
     * 
     * @param {object} _user 
     */
    login(_user: userInfo) {
      this.user = {
        ..._user
      }
      // console.log(this.user)
      const json=JSON.stringify(_user)
      //存储在sessionStorage中
      sessionStorage.setItem('userInfo',json)
      //存储在localStorage中
      localStorage.setItem('userInfo',json)
    },
    /**
     * 退出登录 清除pinia中的用户数据
     */
    logout() {

    },
    /**
     * 解决刷新页面用户登录状态丢失
     * @param {userInfo} userinfo 
     */
    refresh(userinfo: userInfo) {

    }
  },
  getters: {
    /**
     * 根据pinia中的用户数据 判断用户是否已登录
     * @returns 是否已经登录
     */
    isLogin() {
      // console.log('是否已登录')
     //  console.log(!this.user || !this.user.token)
       if ( !this?.user?.token ) return false
      return true;
    }
  }
})