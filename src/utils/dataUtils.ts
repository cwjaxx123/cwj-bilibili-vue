/**
 * 数据处理工具方法集合
 * 
 */
/**
 * 把查询对象转成查询字符串
 * 
 * 
 * @example ?id=10086&name=张三&age=18
 * @param queryobj 
 */
export function GetQueryUrlByObj(queryobj:object){
  
    let queryStr = '?'
    Object.keys(queryobj).forEach(k => {
        queryStr += k + '=' + (queryobj as any)[k] + '&'
    })
//    Object.keys(queryobj).reduce((str,cur)=>{
//    return str+ cur+'='+(queryobj as any)[cur]+'&' 
//    },'?')
    return  queryStr.substring(0,queryStr.length-1)
}