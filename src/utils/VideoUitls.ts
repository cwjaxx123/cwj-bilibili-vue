/**
 * 视频工具方法集合
 * 
 */

import { filetypename, filetypeinfo } from 'magic-bytes.js'
import { upVideoConfig, type upVideo, type FileChunkMd5 } from '@/api/uploadvideo/types'
import { GetFileBasename, GetFileExtname, GetFileMd5Asnc } from '@/utils/FileUtils'
/**
 * 校验上传的视频格式是否符合要求
 * 通过promise异步操作
 * @param files 
 * 
 */
export const upVideoVerify = async (files: File[]) => {
    if (!files || files.length == 0) return await Promise.reject('没有视频文件')
    //校验是否存在不符合的文件类型或者文件名或者文件大小
    /**
     * 校验-是否不合格
     */
    const isNoVer = files.some(file => {
        /**
         * 校验-文件类型
         */
        const r1 = upVideoConfig.video.VideoTypes.map(t => t.toLocaleLowerCase()).includes(GetFileExtname(file.name))
        /**
         * 校验-文件名
         */
        const r2 = !upVideoConfig.video.videonamereg.test(file.name)
        /**
         * 校验-文件名长度
         */
        const r3 = upVideoConfig.video.videonamelen >= GetFileBasename(file.name).length
        /**
         * 校验-文件大小
         */
        const r4 = (Math.floor(file.size / (1024 * 1024)) <= upVideoConfig.video.VideoSizeMax) && (Math.floor(file.size / (1024 * 1024)) >= 10)
        return !(r1 && r2 && r3 && r4)
    })

    if (isNoVer) return await Promise.reject('文件类型或文件名或文件大小 未通过校验,请检查是否符合要求')
    //通过读取文件 验证文件名是否被修改

    for (let file of files) {
        await new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.onloadend = (ff) => {
                const bytes = new Uint8Array(ff.target?.result as ArrayBufferLike);
                const ar = filetypename(bytes)
                //   console.log(new Date())
                if (ar == null || ar.length == 0 || !upVideoConfig.video.VideoTypes.map(t => t.toLocaleLowerCase()).includes('.' + ar[0].toLocaleLowerCase())) {
                    reject('存在文件的文件名被修改过,未通过校验,请检查文件')
                } else {
                    resolve('')
                }
            }
            fileReader.readAsArrayBuffer(file)
        })


    }
    return await Promise.resolve(`${files.length}部视频通过校验!`)
}
/**
 * 异步获取视频的元信息meta
 * 
 * 如:时长 md5 视频切片集合
 *  
 */
export const GetUpVideoMeta = async (video: File): Promise<{ meta: { duration: number }, md5: string, ChunkList: FileChunkMd5[] }> => {
    //视频时长

    const p1 = new Promise((res) => {
        const videoDoc = document.createElement('video')
        const url = URL.createObjectURL(video)

        videoDoc.addEventListener('loadedmetadata', () => {
            // duration = videoDoc.duration

            res(videoDoc.duration)
        })
        videoDoc.src = url
    })

    const p2 = new Promise(async resv => {
        const re = await GetFileMd5Asnc(video)
        resv(JSON.stringify(re))
    })
    return await Promise.all([p1, p2])
        .then(re => {
            const res = JSON.parse(re[1] as string) as { hash: string, ChunkByIndexMd5List: [] }
            return {
                meta: {
                    duration: re[0] as number
                },
                md5: res.hash,
                ChunkList: res.ChunkByIndexMd5List
            }
        })

}

