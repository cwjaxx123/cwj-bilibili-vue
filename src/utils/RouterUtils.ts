/**
 * 路由相关工具方法集合
 */

import router from "@/routers"
const upload = {
    path: 'upload',
    component: () => import('@/views/platform/content/upload'),
    redirect: { name: 'upload_video' },
    children: [
        {
            path: 'video',
            name: 'upload_video',
            component: () => import('@/views/platform/content/upload/video')
        },
        {
            path: 'text',
            component: () => import('@/views/platform/content/upload/text')
        },
        {
            path: 'audio',
            component: () => import('@/views/platform/content/upload/audio')
        }
    ]
}
const upload_manager = {
    path: 'upload-manager',
    component: () => import('@/views/platform/content/upload-manager'),
    redirect: { name: 'upload-manager_article' },
  
    children: [
        {
            path: 'article',
            name: 'upload-manager_article',
            component: () => import('@/views/platform/content/upload-manager/article'),
            redirect: { name: 'upload-manager_article_video' },
            children: [
                {
                    path: 'video',
                    name: 'upload-manager_article_video',
                    component: () => import('@/views/platform/content/upload-manager/article/video'),
                },
                {
                    path: 'audios',
                    component: () => import('@/views/platform/content/upload-manager/article/audios'),
                },
                {
                    path: 'text',
                    component: () => import('@/views/platform/content/upload-manager/article/text'),
                }
            ]
        },
        {
            path: 'appeal',
            component: () => import('@/views/platform/content/upload-manager/appeal')
        },
        {
            path: 'audience-zimu',
            component: () => import('@/views/platform/content/upload-manager/zimu')
        }
    ]
}
const platform = {
    path: '/platform',
    name: 'platform',
    redirect: { name: 'platform_home' },
    component: () => import('@/views/platform/index.vue'),
   
    children: [
        {
            path: 'home',
            name: 'platform_home',
            component: () => import('@/views/platform/content/home')
        },
        upload,
        upload_manager,
        {
            path: 'data-up',
            component: () => import('@/views/platform/content/data-up'),
        },
        {
            path: 'fans',
            component: () => import('@/views/platform/content/fans'),
        },
        {
            path: 'interact-manager',
            component: () => import('@/views/platform/content/interact'),
            children: [
                {
                    path: 'danmu',
                    component: () => import('@/views/platform/content/interact/danmu')
                },
                {

                    path: 'comment',
                    component: () => import('@/views/platform/content/interact/comment')
                }
            ]
        },
        {
            path: 'allowance',
            component: () => import('@/views/platform/content/allowance'),
            children: [
                {
                    path: 'excitation',
                    component: () => import('@/views/platform/content/allowance/excitation'),
                },
                {
                    path: 'center',
                    component: () => import('@/views/platform/content/allowance/center'),
                },
                {
                    path: 'upower-manager',
                    component: () => import('@/views/platform/content/allowance/upower-manager'),
                },
            ]
        }
    ]
}

export const AddRoutes=()=>{
        router.addRoute(platform)
}