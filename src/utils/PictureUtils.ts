/**
 * 图片工具方法集合
 */
import { upVideoConfig } from '@/api/uploadvideo/types'
import { GetFileBasename, GetFileExtname } from '@/utils/FileUtils'
import { filetypename, filetypeinfo } from 'magic-bytes.js'
/**
 * 验证视频封面是否符合要求
 * 成功则返回图片的base64字符串
 * @param cover 
 */
export const CoverFileVerify = async (cover: File) => {
    if (!cover) return await Promise.reject('没有视频封面')
    /**
     * 校验-文件类型
     */
    const r1 = upVideoConfig.picture.PictureTypes
        .map(p => p.toLocaleLowerCase())
        .includes(GetFileExtname(cover.name))
    /**
     * 校验-文件名
     */
    const r2 = upVideoConfig.picture.picturenamereg
        .test(GetFileBasename(cover.name))
    /**
     * 校验-文件名长度
     */
    const r3 = upVideoConfig.picture.picturenameCount >= GetFileBasename(cover.name).length

    /**
     * 校验-文件大小
     */
    const r4 = Math.floor(cover.size / (1024 * 1024)) <= upVideoConfig.picture.pictureSize

    if (!r1 || !r2 || !r3 || !r4) return await Promise.reject('文件类型,文件名,文件大小 未通过校验,请检查是否符合要求')

    let base64 = await new Promise((res, rej) => {
        const fileReader = new FileReader();
        fileReader.onloadend = (f) => {
            const bytes = new Uint8Array(f?.target?.result as ArrayBuffer);
            const ar = filetypename(bytes)
            if (ar == null || ar.length == 0 || !upVideoConfig.picture.PictureTypes
                .map(p => p.toLocaleLowerCase())
                .includes('.' + ar[0].toLocaleLowerCase())) {
                rej('视频封面格式不正确或者未支持')
            } else {
                let fr = new FileReader();
                fr.readAsDataURL(cover)
                fr.onload = (e) => {
                const    base64 = e?.target?.result as string
                    res(base64)
                    // console.log(form.cover)
                }
            }
        }
        fileReader.readAsArrayBuffer(cover)
    })
    return await Promise.resolve(base64)
}