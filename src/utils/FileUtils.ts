/**
 * 文件工具方法集合
 */

import SparkMD5 from 'spark-md5'
import { Md5 } from "ts-md5";
import type { FileChunkMd5 } from '@/api/uploadvideo/types'
/**
 * 由于path模块在浏览器环境不能使用,所以要自己写
 * 
 * 获取文件的扩展名 带.
 * 
 * 如果没有扩展名则返回 空字符串
 * @param path 
 * @param toLowper 是否全部转小写
 */
export const GetFileExtname = (path: string, toLowper: boolean = false): string => {
   const index = path.lastIndexOf('.')
   if (index === -1) return ''
   return toLowper ? path.substring(index).toLocaleLowerCase() : path.substring(index)
}

/**
 * 由于path模块在浏览器环境不能使用,所以要自己写
 * 
 * 获取文件不带扩展名和. 的文件名
 * 
 * 没有扩展名则返回自身
 * @param path 
 * @param toLowper 是否全部转小写
 */
export const GetFileBasename = (path: string, toLowper: boolean = false): string => {
   const DianIndex = path.lastIndexOf('.')
   const index1 = path.lastIndexOf('/')
   const index2 = path.lastIndexOf('\\')
   const startIndex = Math.max(index1, index2) == -1 ? 0 : Math.max(index1, index2) + 1

   if (DianIndex === -1 || DianIndex < startIndex - 1)
      return toLowper ? path.substring(startIndex).toLocaleLowerCase()
         : path.substring(startIndex)


   return toLowper ? path.substring(startIndex, DianIndex).toLocaleLowerCase()
      : path.substring(startIndex, DianIndex)
}

/**
 * 异步获取一个大文件的md5值 hash
 * 
 * 根据文件内容而不是根据文件名获取
 * 
 * 大文件默认切片读取
 * 
 * 默认切片大小为1mb
 * @param file 
 * @param chunkSize 
 * @param retChunkMd5 是否返回切片的MD5及其索引+1 默认返回 
 * @returns 一个promise
 */
export const GetFileMd5Asnc = async (file: File | Blob, chunkSize: number = 1024 * 1024, retChunkMd5: boolean = true)
                                   : Promise<{ hash: string, ChunkByIndexMd5List: FileChunkMd5[] }> => {
   /**
     * 文件切片总数
     */
   const chunkCount = Math.ceil(file.size / chunkSize)
   /**
    * 每个切片的索引及其对应的md5
    */
   const ChunkByIndexMd5List: FileChunkMd5[] = []

   /**
    * promise集合
    */
   const prolist = []
   for (let index = 0; index < chunkCount; index++) {
      const blob = file.slice(index * chunkSize, Math.min(file.size, (index + 1) * chunkSize))
      prolist.push(new Promise((res, rej) => {
         const fileReader = new FileReader()
         const spark = new SparkMD5.ArrayBuffer()
         fileReader.onload = (ee) => {
            spark.append(ee!.target!.result)
            const md5 = spark.end();

            return res(JSON.stringify({ md5, index: index + 1 }))
         }
         fileReader.readAsArrayBuffer(blob)
      })
      )

   }
   return await Promise.all(prolist).then(arr => {

      const hash = arr.reduce((sum: string, ca) => {
         const chuck = JSON.parse(ca as string) as FileChunkMd5
         if (retChunkMd5) ChunkByIndexMd5List.push(chuck)
         return Md5.hashStr(sum + chuck.md5)
      }, '')

      return {
         hash,
         ChunkByIndexMd5List
      }
   })
}

/**
 * 每个文件切片的大小
 */
export const FilechunkSize = 1024 * 1024
