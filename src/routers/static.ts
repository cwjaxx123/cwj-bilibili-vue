import type { RouteRecordRaw,RouteLocationNormalized,NavigationGuardNext } from 'vue-router'
import { useUserStore } from '@/pinia/userStore'
// #region 开发文档路由 正常使用时删除
const dev:RouteRecordRaw[] = [
    {
        path: '/admindev',
        name: 'admindev',
        component: () => import('@/views/AdiminDev')
    }
]
//  #endregion

//#region   静态路由 不用从后台获取

//分区
const category:RouteRecordRaw[] = [
    {
        path: 'movie',
        name: 'moviepage',
        component: () => import('@/views/index/category/moviepage')
    },
    {
        path: 'anime',
        name: 'animepage',
        component: () => import('@/views/index/category/animationpage')
    },
    {
        path: 'v/:category',
        name: 'categorypage',
        component: () => import('@/views/index/category/otherpage')
    }
]
/**
 * 专栏页
 */
import {readpage} from './readpage/index'
//个人空间
import {space} from './space/index'
//流行排行
import {popular} from './popular/index'
import {channelpage} from './channelpage/index'

const main:RouteRecordRaw[] = [
    {
        path: '/',
        redirect: { name: 'homepage' },
        component: () => import('@/views/index'),
        children: [
         
            {
                path: '',
                name: 'homepage',
                component: () => import('@/views/index/homepage')
            },

            {
                path: 'search',
                name: 'searchpage',
                component: () => import('@/views/index/searchpage')
            },
            {
                path: 'play/:id',
                name: 'playpage',
                component: () => import('@/views/index/playpage')
            },
            ...category,
            popular,

            space,
            readpage,
            channelpage,
        ]
    },

]

//#region 创作中心路由 以后需要动态从后台获取
/**
 * 上传路由
 */




//#endregion
/**
 * 总路由
 */
const routes:RouteRecordRaw[] = [
    ...main,

    ...dev,


]
export default routes
