import type { RouteRecordRaw } from 'vue-router'
import { useUserStore } from '@/pinia/userStore'

export const channelpage: RouteRecordRaw = {
    path: 'channel',
    name: 'channelpage',
    component: () => import('@/views/index/channelpage/index.vue'),
    redirect: "/channel/index",
    children: [
        {
            path: 'index',
            component: () => import('@/views/index/channelpage/home.vue')
        },
        {
            path: 'type/:id',
            component: () => import('@/views/index/channelpage/channel_type.vue')

        },
        {
            path: ':id',
            component: () => import('@/views/index/channelpage/channel.vue'),
            beforeEnter: (to: any, from: any, next: any) => {
                if (to.params.id == 'type') {
                    alert('非法路径')
                    next('/channel')
                    return
                }
                next()
            }
        },
        {
            path: 'search',
            component: () => import('@/views/index/channelpage/channelsearch.vue'),
            beforeEnter: (to: any, from: any, next: any) => {
                const text = to.query.text

                if (!text || text.trim() == '') {
                    alert('请输入要寻找的频道')
                    next('/channel')
                    return
                }
                next()
            }
        }
    ]
}