// 路由守卫
import static_routes from './static'
import { usepageconfigStore } from '@/pinia/pageconfig'
import { createRouter, createWebHistory, type RouteLocationNormalized, type NavigationGuardNext, createWebHashHistory } from "vue-router";
import { AddRoutes } from '@/utils/RouterUtils'
import { useUserStore } from '@/pinia/userStore'
const router = createRouter({
  history: createWebHistory(),
  routes: static_routes
})
router.beforeEach((to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {
  // console.log('路由守卫')

  const routes = router.getRoutes()
  // console.log(routes)
  // console.log(to)
  if (!routes.some(r => r.path == to.path || (to.matched.length > 0 && r.path == to.matched[to.matched.length - 1].path))) {
    // console.log('不存在' + to.path)
    AddRoutes()
    next(to.fullPath)
  } else {
    // console.log('存在')
    next()
  }

})
//设置页面状态守卫
router.beforeEach((to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {

  const pageconfig = usepageconfigStore()
  const list = to.matched
  //  list.forEach(li=>{
  //   console.log(li.name)
  //  })
  let i;
  for (i = list.length, i >= 0; i--;) {
    if (list[i]?.name) {
      // console.log('你好'+list[i].name)
      pageconfig.setpagename(list[i].name)
    }
  }
  next()
})
router.beforeEach((to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {
  // console.log("验证是否需要登录")

  const userStore = useUserStore()
  if (to.meta?.needLogin && !userStore.isLogin) {
    alert("需要登录")
    next({ name: 'homepage' })
  } else {
    next()
  }
})
export default router
