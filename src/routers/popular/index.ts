import type { RouteRecordRaw } from 'vue-router'
import { useUserStore } from '@/pinia/userStore'
export const popular = {
    path: 'popular',
    name: 'popularpage',
    component: () => import('@/views/index/popularpage'),
    children: [
        { path: 'all', component: () => import('@/views/index/popularpage/all.vue') },
        { path: 'history', component: () => import('@/views/index/popularpage/history.vue') },
        { path: 'music', component: () => import('@/views/index/popularpage/music.vue') },
        {
            path: 'rank',
            redirect: '/popular/rank/all',
            children: [
                { path: ':ca', component: () => import('@/views/index/popularpage/rank.vue') }
            ]
        },
        { path: 'weekly', component: () => import('@/views/index/popularpage/weekly.vue') },
    ]
}