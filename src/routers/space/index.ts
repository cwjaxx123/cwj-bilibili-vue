import type { RouteRecordRaw } from 'vue-router'
import { useUserStore } from '@/pinia/userStore'

export const space: RouteRecordRaw = {
    path: 'space',
    name: 'spacepage',
    redirect: { name: 'spacepage_home' },
    component: () => import('@/views/index/space'),
    beforeEnter: (to: any, from: any, next: any) => {

        const userStore = useUserStore()
        if (to.query.uid) {
            next()
        } else {
            if (!userStore.isLogin) {
                alert("需要登录")
                next({ name: 'homepage' })
            } else if (userStore.isLogin) {
                next()
            } else if (!to.query.uid && !userStore.isLogin) {
                alert('路径错误')
                next({ name: 'homepage' })
            }
        }
    },
    children: [
        {
            path: 'home',
            name: 'spacepage_home',
            component: () => import('@/views/index/space/routespage/home')
        },
        {
            path: 'contribute',
            component: () => import('@/views/index/space/routespage/contribute'),
            redirect: { name: 'contribute_video' },
            children: [


                {
                    name: 'contribute_video',
                    path: 'video',
                    component: () => import('@/views/index/space/routespage/contribute/video.vue')
                },
                {
                    path: 'audio',
                    component: () => import('@/views/index/space/routespage/contribute/audio.vue')
                },
                {
                    path: 'article',
                    component: () => import('@/views/index/space/routespage/contribute/article.vue')
                },
                {
                    path: 'album',
                    component: () => import('@/views/index/space/routespage/contribute/album.vue')
                }
            ]
        },
        {
            path: 'dynamic',
            component: () => import('@/views/index/space/routespage/dynamic')
        },
        {
            path: 'compilations',
            component: () => import('@/views/index/space/routespage/compilations'),
            redirect: { name: 'compilations_series' },
            children: [
                {
                    path: 'series',
                    name: 'compilations_series',
                    component: () => import('@/views/index/space/routespage/compilations/series.vue')

                },
                {
                    path: ':id',
                    component: () => import('@/views/index/space/routespage/compilations/list.vue')
                }

            ]
        },
        {
            path: 'favlist',
            component: () => import('@/views/index/space/routespage/favlist'),
            redirect: { name: 'favlist_default' },
            children: [
                {
                    path: 'default',
                    name: 'favlist_default',
                    component: () => import('@/views/index/space/routespage/favlist/createcard.vue')
                },
                {
                    path: ':id',
                    component: () => import('@/views/index/space/routespage/favlist/createcard.vue')

                }
            ],
        },
        {
            path: 'subscription',
            component: () => import('@/views/index/space/routespage/subscription'),
            redirect: { name: 'subscription_bangumi' },
            children: [
                {
                    path: 'bangumi',
                    name: 'subscription_bangumi',
                    component: () => import('@/views/index/space/routespage/subscription/bangumi')
                },
                {
                    path: 'cinema',

                    component: () => import('@/views/index/space/routespage/subscription/cinema')
                },
                {
                    path: 'label',
                    component: () => import('@/views/index/space/routespage/subscription/label')
                }
            ]
        },
        {
            path: 'setting',
            component: () => import('@/views/index/space/routespage/setting')
        },

    ]
}