import type { RouteRecordRaw } from 'vue-router'
import { useUserStore } from '@/pinia/userStore'


export const readpage: RouteRecordRaw = {
    name: 'readpage',
    path: 'read',
    component: () => import('@/views/index/readpage'),
    redirect: '/read/home',
    children: [
        {
            path: 'home',
            redirect: '/read/home/recommend',
            children: [
                {
                    path: ':ca',
                    component: () => import('@/views/index/readpage/home'),
                    name: 'read_home'
                },


            ]
        },
        {
            path: 'readlist/:id',
            component: () => import('@/views/index/readpage/readlist.vue'),

        },
        {
            path: ':cid',
            component: () => import('@/views/index/readpage/reading.vue')
        }
    ]
}